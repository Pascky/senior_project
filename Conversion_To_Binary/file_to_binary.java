import java.util.*;
import java.io.*;

public class file_to_binary{


  static void readTextFile(String filename, ArrayList<Integer> array){
    
    String current_line;
    String[] input;

    try{	
      FileReader file_reader = new FileReader(filename);
      BufferedReader buffer = new BufferedReader(file_reader);
      while ((current_line = buffer.readLine()) != null) {
        input = current_line.split(":");
 	array.add( Integer.parseInt(input[1].trim(), 16) );
      }
      buffer.close();	
      }
      catch(IOException e){
        e.printStackTrace();
	System.out.println("Could not write to frequency file");
      }	
  }


  static void writeBinaryFile(String binary_file, ArrayList<Integer> array){
    //File file = new File(binary_file);
    BinaryOut out = new BinaryOut(binary_file);
    for (Iterator<Integer> it = array.iterator(); it.hasNext(); ) {
      int element = it.next();
     // System.out.println(element);
      out.write(element);
    }
    out.close();
    return;
}

public static void main(String[] args) {
  
  ArrayList<Integer> data = new ArrayList<Integer>();
  System.out.println("Reading from text file...\n");
  readTextFile(args[0], data);
  System.out.println("Done reading from disk file!");
   
  System.out.println("Writing to binary file...\n");
  writeBinaryFile(args[1], data);
  System.out.println("Done writing to file!");

}


}
