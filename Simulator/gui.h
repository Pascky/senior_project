#ifndef GUI_H
#define GUI_H

#include <iostream>

#include <QtGui/QApplication>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QtOpenGL/QGLWidget>
#include <QtGui/QKeyEvent>
#include <QtCore/QTimer>
#include <QtGui/QLabel>

class GUI: public QGLWidget {
        Q_OBJECT
        
	public: 
          GUI( QWidget *parent = NULL );
          virtual ~GUI();
       

        private:
          QGroupBox *logoGroupBox;
          QGroupBox *codeRegGroupBox;
          QGroupBox *storageGroupBox;
          QScrollArea *code_area;
          QScrollArea *reg_area;
          QScrollArea *mem_area;
          QScrollArea *disk_area;
          QWidget code_contents;   
          QWidget reg_contents;
          QWidget mem_contents;
          QWidget disk_contents;
          QLabel *mem_label;
          QLabel *disk_label;
          
 
	protected:
          virtual void initializeGL();
	  virtual void resizeGL( int width, int height );
	  virtual void paintGL();
 	  void createCodeRegGroupBox( void );
	  void createStorageGroupBox();
	  virtual void keyPressEvent(QKeyEvent * event);

          QTimer _timer;
	  
	  double _zoom;
	  std::pair< double, double > _center;    
        
        
        
};
#endif /* GUI_H */

