#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>
//#include <GL/glut.h>

#include "gui.h"

using namespace std;




GUI::GUI( QWidget * parent ) : QGLWidget( parent ), 
				    _timer(),
				    _zoom( 5.0 ),
				    _center( 0.0, 0.0 ){


  setMinimumSize( 600, 600 );
  setFocusPolicy(Qt::StrongFocus);
  
  createCodeRegGroupBox( );
  createStorageGroupBox( );
  
  QVBoxLayout *mainLayout = new QVBoxLayout;

  mainLayout->addWidget(codeRegGroupBox);
  mainLayout->addWidget(storageGroupBox);

  setLayout(mainLayout);

  setWindowTitle(tr("Fusion Core Simulator"));


  QTimer *timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(update()));
  timer->start(1000);
}




void GUI::createCodeRegGroupBox( void ){
  codeRegGroupBox = new QGroupBox(tr("Code and Register Values"));
  QHBoxLayout *layout = new QHBoxLayout;

  code_area = new QScrollArea();//tr("Code"));
  layout->addWidget(code_area);
  reg_area = new QScrollArea(); //tr("Registers"));
  layout->addWidget(reg_area);
  codeRegGroupBox->setLayout(layout);
  return;
}


void GUI::createStorageGroupBox( void ){
  storageGroupBox = new QGroupBox(tr("Memory and Disk Contents"));
  QHBoxLayout *layout = new QHBoxLayout;

  mem_area = new QScrollArea();//tr("Memory"));
  layout->addWidget(mem_area);
  disk_area = new QScrollArea();//tr("Disk");
  layout->addWidget(disk_area);
  storageGroupBox->setLayout(layout);
  return;
}



GUI::~GUI(){
}





void GUI::initializeGL(){
  glClearColor( 1.0, 1.0, 1.0, 1.0 );
  glEnable( GL_LINE_SMOOTH );
  glEnable( GL_BLEND );
  return;
}



void GUI::resizeGL( int width, int height ){
  glViewport( 0, 0, width, height );
  return;
}

void GUI::paintGL(){
glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  double ratio = ( double)( size().width() ) / (double)( size().height() );
  gluOrtho2D( -_zoom * ratio + _center.first, _zoom * ratio + _center.first, -
    _zoom + _center.second, _zoom + _center.second );
  glMatrixMode(GL_MODELVIEW);

  glLoadIdentity();
  return;
}






void GUI::keyPressEvent(QKeyEvent * event){
  if( event->matches( QKeySequence::Copy ) ){
    close();
    return;
  }
  else{
    switch (event->key()) {
    case Qt::Key_Left:
      _center.first -= 0.5;
      break;
    case Qt::Key_Right:
	 _center.first += 0.5;
	 break;
    case Qt::Key_Down:
	 _center.second -= 0.5;
	 break;
    case Qt::Key_Up:
	 _center.second += 0.5;
	 break;
    case Qt::Key_I:
	 if ( _zoom > 0.5 ){
	   _zoom -= 0.5;
	 }
	 break;
    case Qt::Key_O: 
	 _zoom += 0.5;
	 break;
    default:
	 cout << "could not handle key " << event->key() << endl;
	 break;
    }
  updateGL();
  }

  return;
}

