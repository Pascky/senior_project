/*
 * Email: smathunjwa@fusion-core.org
 * Description: Provided an ELF file, the program emulates a fusion-core processor. 
 ***********************************************************************************************/

#include <stdio.h>
#include <string>
#include "fusion-core-sim.h"
#include <limits>
//#include "gui.h"

using namespace std;


/****************************** FUNCTIONS *******************************************************/


void wait(){
  std::cout << "Press ENTER to continue...";
  std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
}


/************************************ MAIN ******************************************************/
int main( int argc, char* argv[] ){
  Sim sim; 

  /* Check if enough arguments provided */
  if( argc != 3 ){
    cout << "Usage: ./simulator <1 to step through program, 0 otherwise> <elf file name>" << endl;
    exit(0);
  }


  /* Load Instruction memory */
  int section_size = sim.parse_elf_file( argv[2] );
  if( !section_size ){
    std::cout << "Cannot find ELF file" << std::endl;
    exit(0);
  }

  uint32_t num_insns = section_size/4;
  int step = atoi(argv[1]);
 


  std::cout << "+++++++++++++++++ +++++++++++ Program Started +++++++++++++++++++++++++" << std::endl;
  

  /* Execute code */
  while( sim.PC < num_insns ){
    sim.execute();
    if(step)
      wait();
  }


  sim.write_to_disk( "disk.txt" );


 std::cout << "+++++++++++++++++++++++++ Program Ended! ++++++++++++++++++++++++++++++" << std::endl;
 
  return 0;
}
