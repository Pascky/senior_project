#include<iostream>
#include<fstream>
#include <newt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits> 

using namespace std;
/* newt cleanup stuff */
void cleanupNewt() {
 
    newtFinished();
 
}


void read_text_file(const char* filename, string* text){
 string line;
 ifstream myfile (filename);
   if (myfile.is_open()){
     while ( getline (myfile,line) ){
       *text += line;
	   *text += "\n";
	 }
	 myfile.close();
	 }
     else cout << "Unable to open file";
}


 
/* newt initialization stuff with background title */
void initNewt(unsigned int y, unsigned int* width, unsigned int* height, const char* pTitle) {
    int w, h;
	newtGetScreenSize(&w, &h);
    *width = (unsigned int)w;
	*height = (unsigned int)h;

    newtInit();
    newtCls(); 
    newtDrawRootText(1, y, pTitle);
 
}
 

 
/* draw a centered message box */
void messageBox(unsigned int uiW, unsigned int uiH, const char* pMessage) {
 
    newtComponent form, label, button;
 
    newtCenteredWindow(uiW, uiH, "Message Box");
    newtPopHelpLine();
    newtPushHelpLine(" < Press ok button to return > ");
 
    label = newtLabel((uiW-strlen(pMessage))/2, uiH/4, pMessage);
    button = newtButton((uiW-6)/2, 2*uiH/3, "Ok");
    form = newtForm(NULL, NULL, 0);
    newtFormAddComponents(form, label, button, NULL);
    newtRunForm(form);
 
    newtFormDestroy(form);
    
	return;
}







/* Main routine for the greeter */
int main(int argc, char* argv[]){
 
 /* constant and variable data required */
    const char window_title[] = "Fusion React Simulator";   
	char* pMessage = "End Program";
    unsigned int width, height;
    newtComponent form, code_area, register_area, memory_area; 
   
   /* initialize newt stuff */
    initNewt(1, &width, &height, window_title);

	string code;
    read_text_file("code.txt", &code);
	code_area = newtTextboxReflowed(1, 1, (char*)code.c_str(), width, height, 5, 0);    
	register_area = newtVerticalScrollbar(width, 0, height,NEWT_COLORSET_WINDOW, NEWT_COLORSET_ACTCHECKBOX);
//    newtOpenWindow(width, 1, width, height, "Fusion React Simulator");
    
	form = newtForm(NULL, NULL, 0);
	newtFormAddComponents(form, code_area, register_area, NULL);
    newtRunForm(form);

    messageBox(40, 10, pMessage);
    /* cleanup newt library */
	newtFormDestroy(form);
    cleanupNewt();
 
    return 0;
 
}
