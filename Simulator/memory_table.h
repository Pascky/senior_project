#ifndef MemTable_H
#define MemTable_H

#define MEMORY_SIZE 64
#define NUMBER_OF_REGISTERS 64
#define NUMBER_OF_COLUMNS 32
#define BASE_MEMORY 0x40000

#include <string>
#include <sstream>

#include <QAbstractTableModel>
#include <QTimer>

enum StorageType{ REGISTERS =  0, MEMORY = 1};  

class MemoryTable: public QAbstractTableModel{
  
  Q_OBJECT

  public:
    MemoryTable(QObject *parenti, int* in_data, StorageType st);
    
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
//    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    QTimer* timer;
   


  protected:
    int *_table_data;
    int _num_rows;
    int _num_columns;
};
#endif /* MemTable_H */

