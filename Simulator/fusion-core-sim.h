#ifndef FUSION_CORE_SIM_H
#define FUSION_CORE_SIM_H

#include <cstdint>
#include <iostream>
#include <elfio/elfio.hpp>

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
#define NUM_REGS 64
#define MEM_SIZE 512
#define BASE_MEM 0x400000
#define INSN_MEM_BASE 0x40000

/* Determine Instruction type */
#define IS_R_TYPE(insn) 	( (insn >> 26) == 0x13 )
#define IS_I_TYPE(insn) 	( (insn >> 26) == 0x16 )
#define IS_L_TYPE(insn) 	( (insn >> 26) == 0x1E )
#define IS_LI_TYPE(insn) 	( (insn >> 26) == 0x10 )
#define IS_S_TYPE(insn) 	( (insn >> 26) == 0x1D )
#define IS_J_TYPE(insn) 	( (insn >> 26) == 0x0C )
#define IS_JL_TYPE(insn) 	( (insn >> 26) == 0x04 )
#define IS_B_TYPE(insn) 	( (insn >> 26) == 0x0D )
#define IS_SYS_TYPE(insn) 	( (insn >> 26) == 0x18 )
#define IS_CO_TYPE(insn) 	( (insn >> 26) == 0x3F )
#define IS_NOP(insn) 	( (insn >> 26) == 0x00 )



#define GET_RD_R(x) ( (x >> 21) & 0x1F )
#define GET_RSA_R(x) ( (x >> 16) & 0x1F )
#define GET_RSB_R(x) ( (x >> 11) & 0x1F )
#define GET_FUNCT_R(x)  ( x & 0xF )

#define GET_RD_I(x) ( (x >> 21) & 0x1F )
#define GET_RSA_I(x) ( (x >> 16) & 0x1F )
#define GET_IMM_I(x) ( (x >> 4) & 0xFFF )
#define GET_FUNCT_I(x) (x & 0xF ) 
    
#define GET_RD_L(x) ( (x >> 21) & 0x1F )
#define GET_RSA_L(x) ( (x >> 16) & 0x1F ) 
#define GET_FUNCT_L(x) ( (x >> 14) & 0X3 )
#define GET_IMM_L(x) (x & 0x3FFF)

#define GET_RD_LI(x) ( (x >> 21) & 0x1F )
#define GET_IMM_LI(x) ( x & 0XFFFF ) 
#define GET_FUNCT_LI(x) ( (x >> 17) & 0xF )

#define GET_RSA_S(x) ( (x >> 16) & 0x1F )
#define GET_RSB_S(x) ( (x >> 11) & 0x1F )
#define GET_IMM_S(x) ( ( x & 0xE00000) >> 10 ) | ( x & 0X7FF )  
#define GET_FUNCT_S(x) (( x >> 24 ) & 0x3 )

//#define GET_RSB_J(x) ( (x >> 16) & 0x1F )
//#define GET_IMM_J(x) ( ( x & 0x1F00000) >> 5 ) | ( x & 0XFFFF )
//#define GET_FUNCT_J(x) ( (x >> 26) & 0x3F )
    
#define GET_RSA_B(x) ( ( x >> 16) & 0x1F )
#define GET_RSB_B(x) ( ( x >> 11) & 0x1F )
#define GET_IMM_B(x) ( ( x & 0x3E00000) >> 12 ) | ( (x >> 2) & 0X1FF )
#define GET_FUNCT_B(x) ( x & 0x3 )
    
#define GET_RSA_J(x) ( ( x >> 16) & 0x1F )
#define GET_IMM_J(x) ( ( x & 0x3E00000) >> 5 ) | ( x & 0XFFFF )
#define GET_FUNCT_J(x) ( (x >> 16) & 0x1F )



#define ZERE 0,
#define SP 1
#define FP 2
#define GP 3
#define RA 4
#define ARG0 5
#define ARG1 6
#define ARG2 7
#define ARG3 8 
#define RVAL0 9
#define RVAL1 10
#define GR0 11
#define GR1 12
#define GR2 13
#define GR3 14
#define GR4 15
#define GR5 16
#define GR6 17 
#define GR7 18
#define GR8 19
#define GR9 20
#define GR10 21
#define TMP0 22
#define TMP1 23
#define TMP2 24
#define TMP3 25
#define TMP4 26
#define TMP5 27
#define TMP6 28
#define TMP7 29
#define HI0 30
#define LOW0 31
#define ZERO 32
#define SP1 33
#define FP1 34
#define  GP1 35
#define  RA1 36
#define  SYSARG0 37
#define  SYSARG1 38
#define  SYSARG2 39
#define  SYSARG3 40
#define  SYSARG4 41
#define  SYSARG5 42
#define  SYSRVAL0 43
#define  SYSRVAL1 44
#define  SYSRVAL2 45
#define  SYSRVAL3 46
#define  SYSRVAL4 47
#define  GPR0 48
#define  GPR1 49
#define  GPR2 50
#define  GPR3 51
#define  GPR4 52
#define  GPR5 53
#define  GPR6 54
#define  GPR7 55
#define  SYSTMPR0 56
#define  SYSTMPR1	= 57;
#define  SYSTMPR2	= 58;
#define  SYSTMPR3	= 59;
#define  SYSTMPR4	= 60;
#define  SYSTMPR5	= 61;
#define  SYSREGHI0	= 62;
#define  SYSREGLOW0 	= 63;



using namespace ELFIO;


class Sim{
  public:
    Sim();
    virtual ~Sim();
     
    /* Members */
    int32_t registers[NUM_REGS] = {}; /* 64 32 bit Registers */
    uint8_t data_mem[MEM_SIZE] = {};		/* 1 MB Memory */
    uint32_t* insn_mem;		/* Allocated in main */
    uint32_t PC;                /* Program counter relative to the base instruction memory address */
    uint32_t PC_ABSOLUTE;	/* Actual Program counter */
    uint8_t rd;		/* Destination register address */
    uint8_t rsa;		/* Source register a */
    uint8_t rsb;		/* Source register b */
    uint32_t imm;
    uint32_t insn;
    std::string insn_type;
    std::string insn_name;
    std::string code;

   /* Methods */

    //R Type
    void add( void );
    void sub( void );
    void not_r( void );
    void and_r( void );
    void or_r( void );
    void xor_r( void );
    void sal( void );
    void sar( void );
    void sll( void );
    void slr( void );
    int32_t comp( void );



  //I Type
    void addi( void );
    void subi( void );
    void not_i( void );
    void and_i( void );
    void or_i( void );
    void xor_i( void );
    void sali( void );
    void sari( void );
    void slli( void );
    void slri( void );
    int32_t compi( void );
 


    //Load Type
    void lw( void );
    void lth( void );
    void lh( void );
    void lb( void );




    //LI Type
    void li( void );
    void lsi( void );
    void lgi( void );
    void lui( void );
    void lusi( void );
    void lugi( void );
    void lni( void );
    void lnsi( void );
    void lngi( void );
    void luni( void );
    void lunsi( void );
    void lungi( void );



    //S Type
    void sw( void );
    void sh( void ); 
    void sb( void );
    void sth( void );



    //J Type
    void j( void );
    void jal( void );
    void jr( void );
    void jrl( void );




    //B Type
    void beq( void );
    void bne( void );
    void bgt( void );
    void blt( void );

    //Helper Methods
    void print_insn_details( void );
    void write_to_disk( const char* filename );
    uint32_t parse_elf_file( const char* filename );
    void read_texfile( const char* filename );
    void execute( void );
};
#endif /* FUSION_CORE_SIM_H */
