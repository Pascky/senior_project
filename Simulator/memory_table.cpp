
#include "memory_table.h"


MemoryTable::MemoryTable(QObject *parent, int* in_data, StorageType st):  
                                                         QAbstractTableModel(parent),
		                                         _table_data(in_data),
                                                         _num_columns(NUMBER_OF_COLUMNS),
                                                         _num_rows(MEMORY_SIZE/NUMBER_OF_COLUMNS){
  if(st == REGISTERS){
    _num_columns = 1;
    _num_rows = NUMBER_OF_REGISTERS;
  }
  //    selectedCell = 0;
/*    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()) , this, SLOT(timerHit()));
    timer->start();
*/}


int MemoryTable::rowCount(const QModelIndex &parent ) const{
  return _num_rows;
}

int MemoryTable::columnCount(const QModelIndex &parent) const{
  return _num_columns;
}


QVariant MemoryTable::data(const QModelIndex &index, int role) const{
  if(role == Qt::DisplayRole){
    int idx = index.row() * NUMBER_OF_COLUMNS + index.column(); 
    std::stringstream stream;
    stream << std::hex << _table_data[idx];
    const char* s = stream.str().c_str();
    return QString(s);
  }
  return QVariant();
}



QVariant MemoryTable::headerData(int section, Qt::Orientation orientation, int role) const{
    if (role == Qt::DisplayRole){
        if (orientation == Qt::Horizontal) {
          switch (section){
            case 0:
              return QString("00");
            case 1:
              return QString("01");
            case 2:
              return QString("02");
            case 3:
              return QString("03");
            case 4:
              return QString("04");
            case 5:
              return QString("05");
            case 6:
              return QString("06");
            case 7:
              return QString("07");
            case 8:
              return QString("08");
            case 9:
              return QString("09");
            case 10:
              return QString("0A");
            case 11:
              return QString("0B");
            case 12:
              return QString("0C");
            case 13:
              return QString("0D");
            case 14:
              return QString("0E");
            case 15:
              return QString("0F");
            case 16:
              return QString("10");
            case 17:
              return QString("11");
            case 18:
              return QString("12");
            case 19:
              return QString("13");
            case 20:
              return QString("14");
            case 21:
              return QString("15");
            case 22:
              return QString("16");
            case 23:
              return QString("17");
            case 24:
              return QString("18");
            case 25:
              return QString("19");
            case 26:
              return QString("1A");
            case 27:
              return QString("1B");
            case 28:
              return QString("1C");
            case 29:
              return QString("1D");
            case 30:
              return QString("1E");
            case 31:
              return QString("1F");
            }
        }
        else{
          std::stringstream stream;
          stream << std::hex << BASE_MEMORY + 0;
          const char* s = stream.str().c_str();
          return QString(s);  
        }
    }
    return QVariant();
}
