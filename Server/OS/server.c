
/*
==========================================================================
Name: server_code.c
Author: Sakhile Mathunjwa
Description: Server client communication using TCP protocol
===========================================================================
*/

/**************************** Headers ****************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>


/**************************** CONSTANTS **************************/
#define BUF_SIZE 256
#define QUEUE_DIR "./Server_Queue"
#define PERMISSION 0777


/**************************** METHODS ****************************/
void doProcessing(int sock){
  int n = 0;
  char buffer[BUF_SIZE];

  /* Creat Directory that holds received file is it does not already exist*/
  DIR *dir = opendir(QUEUE_DIR);
  if(dir){  // Directory exists
    closedir(dir);
  }
  else{ //Directory did not exist. Create one.
     if(mkdir(QUEUE_DIR, PERMISSION) == -1){
       perror("Could not create Queue directory");
       exit(EXIT_FAILURE);
     }
  }


  /*Create, write to file then close it*/
  FILE *file_ptr;
  file_ptr = fopen("./Server_Queue/file.txt", "w");
  if(file_ptr == NULL){
    perror("Could not create file to store recieved data");
    exit(EXIT_FAILURE);
  }

  /* Clear and read into buffer */
  bzero(buffer, BUF_SIZE);  
  printf("Lalal");
  n = read(sock,  buffer, BUF_SIZE - 1);
  fwrite ( buffer, sizeof(char), n, file_ptr);
  printf("%d", n);

  if(n < 0){
    fclose(file_ptr);     
    perror("Error reading socket");
    exit(EXIT_FAILURE);
  }
  fclose(file_ptr);     
  
  /* Respond */
  //bzero(buffer, BUF_SIZE);
/* 
  if(fgets(buffer, BUF_SIZE - 1, (FILE *)file_ptr) == NULL){
    printf("Sending empty message....");
  }
 */
  n = write(sock, buffer, 5);

  if(n < 0){
    perror("Error writing to socket");
    exit(EXIT_FAILURE);
  }
  
}



/********************************* Main Method ***********************/
int main(int argc, char** argv){
 /*file descriptor: Stores the values returned by the system calland the accept system call */
  int sockfd;            //file descriptor
  int newsockfd;         //file descriptor
  int portno;            //stores port number on which the server accepts connection
  socklen_t clilen;      //stores the size of the address of the client
  struct sockaddr_in serv_addr;  //address of the server
  struct sockaddr_in cli_addr;   //address of client
  int pid;

  /* Check if enough arguments provided */
  if(argc < 2){
    printf("Lalal");
    fprintf(stderr, "Error, no port provided\n");
    exit(EXIT_FAILURE);
  }

  /*First call to socket() function */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if(sockfd < 0){
    perror("Error opening socket");
    exit(EXIT_FAILURE);
  }

  
  /* Initialise socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = atoi(argv[1]);

  serv_addr.sin_family = AF_INET;              //address family
  serv_addr.sin_addr.s_addr = INADDR_ANY;      //IP address of the host - machine on which server is running
  serv_addr.sin_port = htons(portno);          //convert port number to byte code

  /* Bind the host address using the bind system call */
  if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
      perror("Error on binding");
      exit(EXIT_FAILURE);
    }

  /*Start listening for clients
   *process will go into sleep mode and will wait for incoming connection
   */
  listen(sockfd, 5);   //second arguement is the backlog queue(number of connections that can wait while the process is handling a particular connection
  clilen = sizeof(cli_addr);

  while(1){
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);  //accept system call causes the process to block until a client connects to the server i.e it wakes up the process when a connection to a client has been successfuly estabblished.

    if(newsockfd < 0){
      perror("Error on accept");
      exit(EXIT_FAILURE);
    }

    /*create child process */
    pid = fork();
    
    if(pid < 0){
      perror("Error on the fork");
      exit(EXIT_FAILURE);
     }

    /* client process */
    if(pid == 0){
      close(sockfd);
      doProcessing(newsockfd);
      exit(0);
    }
    else{
      close(newsockfd);
    }

  }

  
  return 0;
}
