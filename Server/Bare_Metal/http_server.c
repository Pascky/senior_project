/************************************************************************************
* Copyright (C) 2018 by Sakhile Mathunjwa - University of Rochester
* 
* Redistribution, modification or use of this software in source or binary forms
* is permiitted to modify as long as the files maintain this copyright. Users are
* permitted to modify this and use it to learn about the field of embedded softaware.
* Sakhile and University of Rochester are not liable for any misuse of this material.
*
*
* This code is part of the Low Power Server Senior Design. This code facilitates 
* communication between an http server and a client via the TCP/IP network protocol. The 
* lwip library's Raw API is used to implement this functionality, the only option 
* available for systems without an operating system.
************************************************************************************/

/************************  TODO **************************************************** 
	1. Process raw Ethernet
      	2. Error handling.
	3. 
*/


/************************ HEADERS AND MACROS *******************************************/
#include "lwip/tcp.h"

#define BUFF_SIZE 1024
#define CODE_MARKER C 
#define DATA_MARKER D 

/************************* GLOBAL VARIABLE ******************************************/
char buffer[BUFF_SIZE];

/************************** FUNCTIONS ***********************************************/ 
/* Server Initialisation */
void http_init(void){
  struct tcp_pcb * pcb;	/* Data Fields: ip_addr_t local_ip
  					struct tcp_pcb *next */
  pcb = tcp_new();	/* Create new connection identifier(PCB) */ 
  if( tcp_bind(pcb, IP_ADDR_ANY, 80) == ERR_USE ){	/* Bind the pcb to a local IP address and port number */
    /* TODO close other connection */
    //printf("Port 80 in use");
  }
  pcb = tcp_listen(pcb);	/* Command PCB to start listenning for incoming connections */ 
  tcp_accept(pcb, http_accept); /* Accept connection. Involves callback function(tcp_connected_fn) */
}



/* Initialise connection state */
static err_t http_accept(void *arg, struct tcp_pcb *pcb, err_t err){
  LWIP_UNUSED_ARG(arg);	/* arg not used */
  LWIP_UNUSED_AGR(err); /* err not used */
  struct http_state *hs;	/* Holds state of connection */

  hs = (struct http_state *) mem_malloc(sizeof(struct http_state));
  if(hs == NULL){
    return ERR_MEM;
  }
  /* Initialise structure */
  hs->file = NULL; 
  hs -> left = 0;
  hs->retries = 0;
  tcp_arg(pcb, hs);	/* hs passed to callback functions */
  tcp_setprio(pcb, TCP_PRIO_MIN);	/* Set connection priority to min */
  tpc_recv(pcb, http_recv); 	/* Call connection_recv() when TCP recieves data (Callback) */
  tcp_err(pcb, NULL);			/* TODO NULL = no function handles fatal errors */
  tcp_poll(pcb, NULL, 4);		/* TODO NULL = no function to call periodically. Poll 4(?) times every sec */
  return ERR_OK;
}


/* Recieves data from client */
static err_t http_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *pbuff, err_t err){
  u32_t i;
  u32_t len;
  u8_t *data;
  struct fs_file file;
  
  tcp_arg(arg);
  hs = (struct http_state *) arg; 
  if(err == ERR_OK){ 
    if (pbuff != NULL){ /* No error and buffer has data */
      tcp_recvd(pcb, pbuff->tot_len); 	/* inform TCP that data has been recieved */
      if(hs->file == NULL){
        data = (u8_t *)pbuff->payload;	/* Point to payload */
        len = pbuff->tot_len;	/* Size of payload */
        if(strncmp(data, "GET ", 4) == 0){ /*Compares the first 4 chars */
          if((data[4] == '/') && (data[5] == '0')){ /* Command is a string */
            fs_open("/index.html", &file); 
          }  
          /* Copy data from file to hs */
          hs->file = file.data;
          hs->left = file.len;
          http_send_data(pcb, hs); /* Send data to client */
          tcp_sent(pcb, NULL); /* Need to be informed about data that has been sent succesfully */ 
        }
      }
    }
    else{	/* If could not allocate buffer, close connection */
      http_close_conn(pcb);
    }
  }
  pbuf_free(pbuff); 	/* Free unneeded buffer */
  return ERR_OK;
} 


/* Sends data to client */
static void http_send(struct tcp_pcb *pcb){
  err_t err;
  u32_t len;

  if(tcp_sndbuf(pcb) < 
}

/* Close connection safely */
static void http_close_conn(struct tcp_pcb *pcb){
  tcp_arg(pcb, NULL);
  tcp_sent(pcb, NULL);
  tcp_recv(pcb, NULL);
  tcp_close(pcb);
  //printf("\nConnection closed! \n");
}
/***************************** MAIN *************************************************/
static void main(){
  /* TODO Process raw Ethernet */
  

  /* Process connections in the rest of the main function */
  
  

}

