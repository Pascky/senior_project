/************************************************************************************
* Copyright (C) 2018 by Sakhile Mathunjwa - University of Rochester
* 
* Redistribution, modification or use of this software in source or binary forms
* is permiitted to modify as long as the files maintain this copyright. Users are
* permitted to modify this and use it to learn about the field of embedded softaware.
* Sakhile and University of Rochester are not liable for any misuse of this material.
*
*
* This code is part of the Low Power Server Senior Design. This code facilitates 
* communication between a server and a client via the TCP/IP network protocol. The 
* lwip library's Raw API is used to implement this functionality, the only option 
* available for systems without an operating system.
************************************************************************************/

/************************  TODO **************************************************** 
	1. Process raw Ethernet
      	2. Error handling.
	3. 
*/


/************************ HEADERS AND MACROS *******************************************/
#include "lwip/tcp.h"

#define BUFF_SIZE 1024
#define CODE_MARKER C 
#define DATA_MARKER D 

/************************* GLOBAL VARIABLE ******************************************/
char buffer[BUFF_SIZE];

/************************** FUNCTIONS ***********************************************/ 
/* Server Initialisation */
void server_init(void){
  struct tcp_pcb * pcb;	/* Data Fields: ip_addr_t local_ip
  					struct tcp_pcb *next */
  pcb = tcp_new();	/* Create new connection identifier(PCB) */ 
  if( tcp_bind(pcb, IP_ADDR_ANY, 80) == ERR_USE ){	/* Bind the pcb to a local IP address and port number */
    /* TODO close other connection */
  }
  pcb = tcp_listen(pcb);	/* Command PCB to start listenning for incoming connections */ 
  tcp_accept(pcb, connection_accept); /* Accept connection. Involves callback function(tcp_connected_fn) */
}



/* Initialise connection state */
static err_t connection_accept(void *arg, struct tcp_pcb *pcb, err_t err){
  LWIP_UNUSED_ARG(arg);	/* arg not used */
  LWIP_UNUSED_AGR(err); /* err not used */
  tcp_arg(pcb, NULL);	/* arg = NULL passed to callback functions */
  tcp_setprio(pcb, TCP_PRIO_MIN);	/* Set connection priority to min */
  tpc_recv(pcb, connection_recv); 	/* Call connection_recv() when TCP recieves data (Callback) */
  tcp_err(pcb, NULL);			/* TODO NULL = no function handles fatal errors */
  tcp_poll(pcb, NULL, 4);		/* TODO NULL = no function to call periodically. Poll 4(?) times every sec */
  return ERR_OK;
}


/* Recieves data from client */
static err_t connection_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *pbuff, err_t err){
  u32_t i;
  u32_t len;
  u8_t *data;
  
  LWIP_UNUSED_ARG(arg);
  if(err == ERR_OK){ 
    if (pbuff != NULL){ /* No error and buffer has data */
      tcp_recvd(pcb, pbuff->tot_len); 	/* inform TCP that data has been recieved */
      data = (u8_t *)pbuff->payload;	/* Point to payload */
      len = pbuff->tot_len;	/* Size of payload */
      /* TODO: Recieve data properly */
      for(i = 0; i < len; i++){	/* Copy recieved data into buffer. Maximum size of 1MB */
        buffer[i] = data[i];
      }
    }
    else{	/* If could not allocate buffer, close connection */
      connection_close(pcb);
    }
  }
  pbuf_free(pbuff); 	/* Free unneeded buffer */
  return ERR_OK;
} 


/* Sends data to client */
static void connection_send(struct tcp_pcb *pcb){
  err_t err;
  u32_t len;

  /*TODO*/
}

/* Close connection safely */
static void connection_close(struct tcp_pcb *pcb){
  tcp_arg(pcb, NULL);
  tcp_sent(pcb, NULL);
  tcp_recv(pcb, NULL);
  tcp_close(pcb);
  //printf("\nConnection closed! \n");
}
/***************************** MAIN *************************************************/
static void main(){
  /* TODO Process raw Ethernet */
  

  /* Process connections in the rest of the main function */
  
  

}

