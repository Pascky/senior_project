#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <netinet/in.h>

#include <string.h>

#define BUFFER_SIZE 256   //in bytes

int main(int argc, char *argv[]){
  int sockfd, portno;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  char buffer[BUFFER_SIZE];
  
/* Ensure user has entered expected number of arguments */
  if(argc < 3){
    fprintf(stderr, "Usage %s hostname port\n", argv[0]);
    exit(EXIT_FAILURE);
  }

/* Store port number provided by user */
  portno = atoi(argv[2]);

 /* Create a socket point. Check if opened successfully */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0){
    perror("Error open socket");
    exit(EXIT_FAILURE);
  } 

 /* get address for given host name */
  server = gethostbyname(argv[1]);

/* Report error if host not found */
  if(server == NULL){
    fprintf(stderr, "Error no such host\n");
    exit(EXIT_FAILURE);
  }
  
  /* Clear all contents of serv_add(struct sockaddr_in) */
  bzero((char *)&serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr, server->h_length);
  serv_addr.sin_port = htons(portno);

/* Now connect to server */
if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))< 0){
  perror("Error connecting");
    exit(EXIT_FAILURE);
  }
  
  /*TODO Ask for message from user. Message to be read by server*/
  printf("Enter message: ");
  bzero(buffer, BUFFER_SIZE);
  if(fgets(buffer, BUFFER_SIZE - 1, stdin) == NULL){
    printf("Sending an empty message...");
  }
  
  /* send message to the server */
  if(write(sockfd, buffer, strlen(buffer))< 0){
    perror("Error writing to socket");
    exit(EXIT_FAILURE);
  }

 /* Read server response */
 bzero(buffer, BUFFER_SIZE);

 if (read(sockfd, buffer, BUFFER_SIZE - 1) < 0){
    perror("Error reading from socket");
    exit(EXIT_FAILURE);
  }

  printf("%s\n", buffer);
  return 0;
}
 
